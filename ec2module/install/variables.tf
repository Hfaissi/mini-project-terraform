variable "region" {default = "us-east-2"}
variable "size" {default = "20"}
variable "ami_id" {default = "ami-024e6efaf93d85776"}

variable instancetype {
  type        = string
  description = "set aws instance type"
  default     = "t2.micro"
}

variable sg_name {
  type        = string
  description = "set sg name "
  default     = "devops-sg"
}

variable aws_common_tag {
  type        = map
  description = "Set aws tag"
  default = {
    Name = "ec2-devops"
  }
}
