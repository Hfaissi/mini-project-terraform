provider "aws" {
    region = var.region
    access_key = "my access key"    
    secret_key = "my secret key"
}

  resource "aws_instance" "ec2" {
  ami = var.ami_id
 // ami             = "ami-0323c3dd2da7fb37d"  # AMI ID for Ubuntu Bionic (18.04 LTS) in your region
  instance_type   = var.instancetype
  key_name        = "devops"
  tags            = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_ssh_http_https.name}"]
  ebs_block_device {
   device_name = "/dev/sda1"  # Update with the desired device name
   volume_size              = var.size            # Specify the size of the EBS volume in gigabytes
   volume_type              = "gp2"         # Specify the volume type (e.g., gp2, io1, st1, sc1)
   }
  user_data = file("../ec2module/install/nginx.sh")
 
   }

resource "aws_security_group" "allow_ssh_http_https" {
  name        = var.sg_name
  description = "Internet reaching access for EC2 Instances"

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "lb" {
  instance = aws_instance.ec2.id
  domain   = "vpc"
  provisioner "local-exec" {
    command = "echo PUBLIC IP: ${aws_eip.lb.public_ip} ; ID: ${aws_instance.myec2.id} ; AZ: ${aws_instance.myec2.availability_zone}; >> infos_ec2.txt"
 
  }
}
